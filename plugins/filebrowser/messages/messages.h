#ifndef BEDIT_FILE_BROWER_MESSAGES_MESSAGES_H
#define BEDIT_FILE_BROWER_MESSAGES_MESSAGES_H

#include "bedit-file-browser-message-activation.h"
#include "bedit-file-browser-message-add-filter.h"
#include "bedit-file-browser-message-extend-context-menu.h"
#include "bedit-file-browser-message-get-root.h"
#include "bedit-file-browser-message-get-view.h"
#include "bedit-file-browser-message-id-location.h"
#include "bedit-file-browser-message-id.h"
#include "bedit-file-browser-message-set-emblem.h"
#include "bedit-file-browser-message-set-markup.h"
#include "bedit-file-browser-message-set-root.h"

#endif /* BEDIT_FILE_BROWER_MESSAGES_MESSAGES_H */

