# __init__.py
# This file is part of Bedit.
#
# Copyright (C) 2020 - Ben Mather
#
# Based on plugins/quickopen/quickopen/__init__.py from Gedit.
#
# Copyright (C) 2009 - Jesse van den Kieboom
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

import os

import gi

gi.require_version("Bedit", "3.0")
gi.require_version("Gtk", "3.0")
from gi.repository import GObject, Gio, Gtk, Bedit

from .popup import Popup

try:
    import gettext

    gettext.bindtextdomain("bedit")
    gettext.textdomain("bedit")
    _ = gettext.gettext
except:
    _ = lambda s: s


class QuickOpenAppActivatable(GObject.Object, Bedit.AppActivatable):
    app = GObject.Property(type=Bedit.App)

    def __init__(self):
        GObject.Object.__init__(self)

    def do_activate(self):
        self.app.add_accelerator("<Primary><Alt>O", "win.quickopen", None)

        self.menu_ext = self.extend_menu("file-section")
        item = Gio.MenuItem.new(_("Quick Open…"), "win.quickopen")
        self.menu_ext.prepend_menu_item(item)

    def do_deactivate(self):
        self.app.remove_accelerator("win.quickopen", None)


class QuickOpenPlugin(GObject.Object, Bedit.WindowActivatable):
    __gtype_name__ = "QuickOpenPlugin"

    window = GObject.Property(type=Bedit.Window)

    def __init__(self):
        GObject.Object.__init__(self)

    def do_activate(self):
        self._popup_size = (450, 300)
        self._popup = None

        action = Gio.SimpleAction(name="quickopen")
        action.connect("activate", self.on_quick_open_activate)
        self.window.add_action(action)

    def do_deactivate(self):
        self.window.remove_action("quickopen")

    def get_popup_size(self):
        return self._popup_size

    def set_popup_size(self, size):
        self._popup_size = size

    def _create_popup(self):
        paths = []

        paths.append(self.window.get_property("default-location"))

        self._popup = Popup(self.window, paths, self.on_activated)
        self.window.get_group().add_window(self._popup)

        self._popup.set_default_size(*self.get_popup_size())
        self._popup.set_transient_for(self.window)
        self._popup.set_position(Gtk.WindowPosition.CENTER_ON_PARENT)
        self._popup.connect("destroy", self.on_popup_destroy)

    # Callbacks
    def on_quick_open_activate(self, action, parameter, user_data=None):
        if not self._popup:
            self._create_popup()

        self._popup.show()

    def on_popup_destroy(self, popup, user_data=None):
        self.set_popup_size(popup.get_final_size())

        self._popup = None

    def on_activated(self, gfile, user_data=None):
        Bedit.commands_load_location(self.window, gfile, None, -1, -1)
        return True
